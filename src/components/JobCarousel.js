/* eslint-disable jsx-a11y/alt-text */
import Carousel from "react-multi-carousel";
import JobCard from "./jobCard";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    paritialVisibilityGutter: 60,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    paritialVisibilityGutter: 50,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    paritialVisibilityGutter: 30,
  },
};

const Simple = ({ deviceType, jobs }) => {
  return (
    <>
      {jobs.length !== 0 ? (
        <Carousel
          ssr
          partialVisbile
          centerSlidePercentage
          deviceType={deviceType}
          itemClass="image-item"
          responsive={responsive}
        >
          {jobs.map((job, index) => {
            return <JobCard key={index} data={job} />;
          })}
        </Carousel>
      ) : (
        <h1>NO JOBS FOUND</h1>
      )}
    </>
  );
};

export default Simple;
