import React from "react";
import Section from "../../components/section";
import UAParser from "ua-parser-js";
import JobCarousel from "../../components/JobCarousel";
import apiAxios from "../../utils/apiAxios";
import { JOBS } from "../../utils/constant";
import { Container } from "react-bootstrap";
import Filter from "../../components/filter";
import NavBar from "../../components/navBar";

const Index = (props) => {
  const [showCompany, setShowCompany] = React.useState(false);
  const [lastSevenDays, setLastSevenDays] = React.useState(false);
  const [filteredCompany, setFilteredCompany] = React.useState(null);

  const getFilter = (companyName, filteredCompany, postedDate) => {
    if (filteredCompany !== null && lastSevenDays === true) {
      return (
        companyName === filteredCompany || parseInt(postedDate.charAt(0) < 7)
      );
    } else if (filteredCompany !== null && lastSevenDays === false) {
      return companyName === filteredCompany;
    } else if (filteredCompany === null && lastSevenDays === true) {
      return parseInt(postedDate.charAt(0) < 7);
    }
  };

  const jobs =
    filteredCompany === null
      ? props.data
      : props.data.filter((e) =>
          getFilter(e.companyName, filteredCompany.name, e.postedDate)
        );

  var size = 10;
  var items = jobs.slice(0, size).map((jobs) => {
    return jobs;
  });

  return (
    <div>
      <NavBar />
      <Container>
        <div className="wrapper">
          <Filter
            onShowCompany={() =>
              setShowCompany((prevState) => (prevState === true ? false : true))
            }
            showCompany={showCompany}
            data={props.data}
            setFilteredCompany={setFilteredCompany}
            setShowCompany={setShowCompany}
            onSetSevenDays={() =>
              setLastSevenDays((prevState) =>
                prevState === true ? false : true
              )
            }
            setLastSevenDays={setLastSevenDays}
            filteredCompany={filteredCompany}
            lastSevenDays={lastSevenDays}
          />
          <span>
            showing {items.length}{" "}
            {filteredCompany ? <b>{filteredCompany.name}</b> : ""} jobs{" "}
            {lastSevenDays ? "posted last seven days" : ""}
          </span>
          <Section>
            <JobCarousel deviceType={props.deviceType} jobs={items} />
          </Section>
        </div>
      </Container>
    </div>
  );
};

Index.getInitialProps = async ({ req }) => {
  let userAgent;
  if (req) {
    userAgent = req.headers["user-agent"];
  } else {
    userAgent = navigator.userAgent;
  }
  const parser = new UAParser();
  parser.setUA(userAgent);
  const result = parser.getResult();
  const deviceType = (result.device && result.device.type) || "desktop";

  const payload = {
    companySkills: true,
    dismissedListingHashes: [],
    fetchJobDesc: true,
    jobTitle: "Business Analyst",
    locations: [],
    numJobs: 20,
    previousListingHashes: [],
  };

  const res = await apiAxios({ url: JOBS, method: "POST", data: payload });

  return {
    deviceType,
    data: res.jobs,
  };
};

export default Index;
