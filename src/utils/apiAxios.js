import axios from "axios";
import { baseUrl } from "./constant";
const apiAxios = async ({
  url,
  method,
  headers = null,
  params,
  data = null,
}) => {
  const config = {
    method: method || "GET",
    headers: {
      ...headers,
      Accept: "application/json",
    },
    params,
    data,
  };

  return axios(baseUrl + url, config)
    .then((response) => response.data)
    .catch((error) => error.response.data);
};

export default apiAxios;
