import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import "react-multi-carousel/lib/styles.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
