import React from "react";
import { Button } from "react-bootstrap";

const CompanyFilter = (props) => {
  const { data, setFilteredCompany } = props;

  const removeDuplicateCompany = data.filter(
    (v, i, a) => a.findIndex((t) => t.companyID === v.companyID) === i
  );

  const filtered = removeDuplicateCompany.map((job) => {
    return {
      name: job.companyName,
      companyID: job.companyID,
    };
  });

  return (
    <div>
      {filtered.map((company, index) => {
        return (
          <Button
            key={index}
            className="m-1"
            onClick={() =>
              setFilteredCompany({ id: company.companyID, name: company.name })
            }
          >
            {company.name}
          </Button>
        );
      })}
    </div>
  );
};

export default CompanyFilter;
