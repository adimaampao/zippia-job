import React from "react";

const NavBar = () => {
  return (
    <nav className="navbar navbar-dark bg-dark">
      <div className="container">
        <h1 className="navbar-brand">BUSINESS ANALYST JOBS</h1>
      </div>
    </nav>
  );
};

export default NavBar;
