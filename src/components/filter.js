import React from "react";
import { Button } from "react-bootstrap";
import CompanyFilter from "./companyFilter";

const Filter = (props) => {
  const {
    data,
    onShowCompany,
    showCompany,
    setFilteredCompany,
    setShowCompany,
    onSetSevenDays,
    setLastSevenDays,
    filteredCompany,
    lastSevenDays,
  } = props;
  return (
    <div>
      <label>Filter:</label>
      <Button className="m-2" onClick={() => onShowCompany()}>
        Company
      </Button>
      <Button className="m-2" onClick={() => onSetSevenDays()}>
        Last 7 days
      </Button>
      {filteredCompany !== null || lastSevenDays === true ? (
        <Button
          className="m-2"
          onClick={() => {
            setFilteredCompany(null);
            setShowCompany(false);
            setLastSevenDays(false);
          }}
        >
          Reset
        </Button>
      ) : null}

      {showCompany ? (
        <CompanyFilter
          data={data}
          setFilteredCompany={setFilteredCompany}
        ></CompanyFilter>
      ) : null}
    </div>
  );
};

export default Filter;
