import React from "react";
import { Card } from "react-bootstrap";

const JobCard = (props) => {
  const {
    data: { jobTitle, companyName, shortDesc, postedDate },
  } = props;

  return (
    <Card style={{ width: "18rem" }}>
      <Card.Body>
        <Card.Title className="text-primary">{companyName}</Card.Title>
        <h5>{jobTitle}</h5>
        <Card.Text className="text-secondary">{shortDesc}</Card.Text>
        {/* <Button variant="primary">Go somewhere</Button> */}
        <hr></hr>
        <h6>{postedDate}</h6>
      </Card.Body>
    </Card>
  );
};

export default JobCard;
